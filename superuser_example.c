/* 
 * Teste da nova chamada de sistema
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

int main(int argc, char* argv[]) {
  if(argc < 3)
    return -1;

  int pid = getpid();
  int aux = getuid();
  int uid;
  
  sscanf(argv[2], "%d", &uid);

  printf("PID: %d   UID inicial: %d   ", pid, aux);
	
  FILE *f = fopen(argv[1], "w");
  if(f == NULL)
    printf("Nao abriu: %s  ", strerror(errno));
  else fclose(f);

  scanf("%d", &aux);
  int r = syscall(351, uid);
  printf("UID final: %d  ", getuid());

  f = fopen(argv[1], "w");
  if(f == NULL){
    printf("Nao abriu: %s  ", strerror(errno));
    return -3;
  }

  fprintf(f, "HAHAHAHAHA, alterei seu arquivo sem ser rootn");

  fclose(f);

  printf("Aparentemente eu pude alterar o arquivo protegido  ");

  scanf("%d", &aux);
  return 0;
}

